(ns meowdule.java_test
  (:require [clojure.test :refer :all]
            [clojure.java.io :refer [as-url]]
            [meowdule.java :refer :all]))

(deftest with-props-test
  (testing
    (is (= {:test 'test :host "example.com" :this-is-path "/foo/bar"}
           (with-props
             (as-url "http://example.com/foo/bar")
             [host path]
             {:test 'test
              :host host
              :this-is-path path})))))
