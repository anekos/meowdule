(ns meowdule.regex_test
  (:require [clojure.test :refer :all]
            [meowdule.regex :refer :all]))

(deftest re-case-test
  (testing
    (is (= ['first "foo is not bar" "is" "not" "bar" nil]
           (re-case
             "foo is not bar"
             #"foo (\S+) (\S+) (\S+)" [whole a b c]
             ['first whole a b c nil]
             #"bar (\S+) (\S+) (\S+)" [whole a b c]
             ['second whole a b c nil]
             'otherwise)))

    (is (= ['second "bar is a bar" "is" "a" "bar" nil]
           (re-case
             "bar is a bar"
             #"foo (\S+) (\S+) (\S+)" [whole a b c]
             ['first whole a b c nil]
             #"bar (\S+) (\S+) (\S+)" [whole a b c]
             ['second whole a b c nil]
             'otherwise)))

    (is (= 'otherwise
           (re-case
             "BAZ is not bar"
             #"foo (\S+) (\S+) (\S+)" [whole a b c]
             ['first whole a b c nil]
             #"bar (\S+) (\S+) (\S+)" [whole a b c]
             ['second whole a b c nil]
             'otherwise)))))
