(ns meowdule.macros_test
  (:require [clojure.test :refer :all]
            [meowdule.macros :refer :all]))

(deftest $->-test
  (testing
    (is (= -2
           ($->
             1
             (+ 1)
             (- 4))))

    (is (= 2
           ($->
             1
             (+ 1)
             (- 4 $))))

    (is (= 2
           ($->
             inc
             ($ 1))))

    (is (= "A"
           (with-out-str
             ($->
               (print "A")
               (list $ $)))))

    #_(is (= {:foo 2}
           ($->
             1
             (+ 1)
             {:foo $})))))

