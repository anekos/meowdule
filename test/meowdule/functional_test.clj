(ns meowdule.functional_test
  (:require [clojure.test :refer :all]
            [meowdule.functional :refer :all]))

(deftest const-test
  (testing "const is constant"
    (is (= ((const 616) 666)
           616))))
