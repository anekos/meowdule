(ns meowdule.string_test
  (:require [clojure.test :refer :all]
            [meowdule.string :as ms]))

(deftest drop-test
  (testing
    (is (= "-infix-suffix"
           (ms/drop "prefix"
                    "prefix-infix-suffix")))

    (is (= "-infix-suffix"
           (ms/drop (count "prefix")
                    "prefix-infix-suffix")))))


(deftest take-test
  (testing
    (is (= "hoge"
           (ms/take 4 "hoge-moge")))

    (is (= "hoge"
           (ms/take 10 "hoge")))))


(deftest remove-prefix-test
  (testing
    (is (= "-infix-suffix"
           (ms/remove-prefix "prefix" "prefix-infix-suffix")))

    (is (= "prefix-infix-suffix"
           (ms/remove-prefix "suffix" "prefix-infix-suffix")))

    (is (= "prefix-infix-suffix"
           (ms/remove-prefix "not-prefix" "prefix-infix-suffix")))))


(deftest remove-suffix-test
  (testing
    (is (= "prefix-infix-"
           (ms/remove-suffix "suffix" "prefix-infix-suffix")))

    (is (= "prefix-infix-suffix"
           (ms/remove-suffix "prefix" "prefix-infix-suffix")))

    (is (= "prefix-infix-suffix"
           (ms/remove-suffix "not-suffix" "prefix-infix-suffix")))))
