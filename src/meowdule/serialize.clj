(ns meowdule.serialize
  (:require [clojure.java.io :refer [file reader writer]]
            [meowdule.functional :refer :all]))


(defn write-to-file [filepath obj]
  (with-open [w (writer filepath)]
    (binding [*out* w]
      (pr obj))
    obj))


(defn read-from-file
  ([filepath]
    (read-from-file filepath (const nil)))
  ([filepath default-ctor]
    (let [cache-file (file filepath)]
      (if (.exists cache-file)
        (with-open [r (java.io.PushbackReader. (reader cache-file))]
          (binding [*read-eval* false]
            (read r)))
        (write-to-file
          filepath
          (default-ctor))))))
