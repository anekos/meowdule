(ns meowdule.cache.simple
  (:require [clojure.java.io :refer [file reader writer]]
            [meowdule.serialize :refer :all]
            [meowdule.functional :refer :all]
            [meowdule.macros :refer [tap]])
  (:refer-clojure :exclude [set get set!]))


(defn make
  ([filepath]
    (make filepath (const {})))
  ([filepath initial-data-ctor]
    (let [cache-file (file filepath)]
      (ref
        {:filepath filepath
         :data (read-from-file
                 cache-file
                 initial-data-ctor)}))))


(defn freeze [cache]
  (let [{filepath :filepath data :data} @cache]
    (write-to-file filepath data)))


(defn get [cache k & [default]]
  (let [{data :data} @cache]
    (if (contains? data k)
      (get data k)
      default)))


(defn get-or-update [cache k updater]
  (dosync
    (let [{data :data :as c} @cache]
      (if (contains? data k)
        (clojure.core/get data k)
        (tap (updater k) it
             (ref-set cache
                      (assoc-in c [:data k]
                                it)))))))
