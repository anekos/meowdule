(ns meowdule.string
  (:require [clojure.string :as string])
  (:refer-clojure :exclude [drop take reverse]))


(declare remove-prefix)


(defmulti drop (fn [n _] (class n)))

(defmethod drop Number [n s]
  (apply str
         (clojure.core/drop n s)))

(defmethod drop String [prefix s]
  (remove-prefix prefix s))


(defn take [n s]
  (apply str
         (clojure.core/take n s)))


(defn reverse [s]
  (apply str
         (clojure.core/reverse s)))


(defn remove-prefix [prefix s]
  (if (.startsWith s prefix)
    (.replaceFirst s prefix "")
    s))


(defn remove-suffix [suffix s]
  (reverse
    (remove-prefix
      (reverse suffix)
      (reverse s))))
