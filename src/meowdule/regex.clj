(ns meowdule.regex)

; (re-case
;   "bar is not foo"
;   #"foo (\S+) (\S+) (\S+)" [_ a b c]
;   (println a b c)
;   #"bar (\S+) (\S+) (\S+)" [_ a b c]
;   (println a b c)
;   'otherwise)

(defn transform [s [re bindings form & rest-clauses]]
  (if bindings
    `(if-let [~bindings (re-matches ~re ~s)]
       ~form
       ~(transform s rest-clauses))
    re))

(defmacro re-case [s & clauses]
  (transform s clauses))
