(ns meowdule.seq)


(defn zip [& ms]
  (apply mapv (cons vector ms)))


(defn flatten1 [x]
  (apply concat x))


(defn index-of [x coll]
  (.indexOf coll x))


(defn sample [coll]
  (loop [result (first coll)
         coll (rest coll)
         n 2]
    (if (empty? coll)
      result
      (let [[x & xs] coll
            sel (if (<= (rand n) 1) x result)]
        (recur sel xs (inc n))))))
