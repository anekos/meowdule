(ns meowdule.java
  (:import java.io.StringReader
           java.io.BufferedReader)
  (:require [clojure.string :as string]
            [meowdule.macros :as macros]))


(defmacro java-to-map
  "(java-to-map obj [foo bar])
  equals
   {:foo (.getFoo obj)
    :bar (.getBar obj)}"
  [obj props]
  (letfn [(key-to-getter [key]
            (symbol
              (str
                ".get"
                (string/replace (string/capitalize (name key))
                                #"-(\w+)"
                                #(string/capitalize (apply str (next %)))))))]
    `(apply
       merge
       {}
       ~@(mapv (fn [key] {(keyword key) (list (key-to-getter key) obj)})
               props))))


(defmacro with-props [obj bindings & body]
  `(macros/with-keys
     (java-to-map ~obj ~bindings)
     [~@bindings]
     ~@body))


(defn string-reader [s]
  (BufferedReader. (StringReader. s)))
