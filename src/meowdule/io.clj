(ns meowdule.io
  (:require [clojure.string :as string]))


(defn read-lines [path]
  (string/split-lines
    (slurp path)))
