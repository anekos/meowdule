(ns meowdule.sugar)


(defmacro tap
  "(tap (foo-bar)
     (println it)) returns (foo-bar)"
  [obj & body]
  `(let [~'it ~obj]
     ~@body
     ~'it))
