(ns meowdule.macros)


(comment
  $->
  init
  (nth 5)
  (map second $))

; TODO for {$ ...}
(defmacro $-> [initial-val & clauses]
  (letfn [($->-replace [clause rep]
            (if (sequential? clause)
              (map #($->-replace % rep) clause)
              (if (= '$ clause)
                rep
                clause)))

          ($->-transform [init clause]
            (let [init-sym (gensym)
                  replaced ($->-replace clause init-sym)]
              (if (= replaced clause)
                (if (list? clause)
                  (let [[head & tail] clause]
                    `(~head ~init ~@tail))
                  `(~clause ~init))
                `(let [~init-sym ~init]
                   ~replaced))))

          ($->-wrap [init clauses]
            (if (empty? clauses)
              init
              (let [[fst & rst] clauses]
                ($->-wrap
                  ($->-transform
                    init
                    fst)
                  rst))))]
    ($->-wrap initial-val clauses)))


(defmacro with-keys
  "(with-keys {:eyes 2, :tails 9, :legs 4}
     [tails eyes]
     (format \"The fox has %d eyes and %d tails\" eyes tails))"
  [m key-names & body]
  (let [mv (gensym "m")]
    `(let [~mv ~m
           [~@key-names] [~@(map #(list (keyword %) mv) key-names)]]
       ~@body)))


(defmacro aif [condition true-clause false-clause]
  `(let [~'it ~condition]
     (if ~'it
       ~true-clause
       ~false-clause)))


(defmacro prog1 [return-form & rest-forms]
  `(let [result# ~return-form]
     ~@rest-forms
     result#))

(defmacro prog2 [first-form return-form & rest-forms]
  `(let [ignore# ~first-form
         result# ~return-form]
     ~@rest-forms
     result#))


(defmacro tap [val-form binding & body]
  `(let [~binding ~val-form]
     ~@body
     ~binding))
