(ns user
  (:require [clojure.tools.namespace.repl :refer [refresh]]
            [clojure.repl :refer :all]
            [clojure.pprint :refer [pp pprint cl-format]]
            [meowdule.sugar :as sugar]
            [meowdule.macros :refer :all]
            [meowdule.serialize :as ser]
            [meowdule.string :as ms]
            [meowdule.regex :refer :all]
            [meowdule.functional :refer :all]
            [meowdule.java :as java]
            [meowdule.cache.simple :as cache]))


(defn init []
  (alter-var-root (var *print-level*) (fn [_] 2))
  (alter-var-root (var *print-length*) (fn [_] 10)))


(defn reset []
  (init)
  (refresh))


(init)
